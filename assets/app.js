/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

/** vue-toastr (notifications library) **/
import Toastr from 'vue-toastr';
Vue.use(Toastr);

/***  Routing library ***/
import VueRouter from 'vue-router';
Vue.use(VueRouter);

/** Import components as tag HTML **/
// Admin components
// Vue.component('add-item', require('./components/AddItemComponent.vue').default);

// Front components
Vue.component('about', require('./components/About.vue').default);


/*-- Import component as endpoint routes --*/
import Vue from 'vue';
import Home from './components/Home'

/*-- Setting Routes components --*/
const routes = [
  {
      path: '/',
      component: Home
  }
  // Protected route example
  // {
  //   path: '/about',
  //   component: UpdateAbout,
  //   meta: {
  //       requiresAuth: true // Set the protected route
  //   }
  // }
];

const router = new VueRouter({routes}); // assign routes conf

/*
// Protected routes
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // Make /item not available if not logged in
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if ( !localStorage.getItem('access_token') ) {
      console.log('Not login ');
      next({
        path: '/login'
      })
    } else {
        console.log('Login !! ');
      next()
    }

  } else if (to.matched.some(record => record.meta.requiresVisitor)) {
  // Make /login not available if logged in
  // this route requires auth, check if logged in
  // if logged in, redirect to home page.
  if ( localStorage.getItem('access_token') ) {
      console.log('Not login ');
      next({
      path: '/'
      })
  } else {
      console.log('Login !! ');
      next()
  }

  } else {
    next() // make sure to always call next()!
  }
})*/

new Vue({
    el: '#app',
    router: router
  });