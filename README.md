
# Start project
* Docker up ```docker-compose up -d```   
* Inside php container```docker exec -it cat-care-php-fpm bash```   

# From container PHP
## Fisrt setup
* Copy/paste .env.test > .env   
* ```composer install```   
* ```yarn```

# Server & compile VueJs to local dev
// * ```yarn encore dev-server --hot```   
* ```yarn encore dev --watch```   

## Create user
```php bin/console user:create admin@mail.com --super-admin```   
> localhost/login

## Change password
```php bin/console user:change-password admin@mail.com```   

!user super admin dev (Login : derrecalde@gmail.com, passwd : admin)!